package com.knudsen;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.JAXBException;

public class UnmarshalTest {
    public static void main(String args[]) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(Config.class);

        Unmarshaller um = jc.createUnmarshaller();
        Config config = (Config) um.unmarshal(new File(args[0]));

        System.out.println("Foo: " + config.getFoo());
        System.out.println("Bar: " + config.getBar());
    }
}
