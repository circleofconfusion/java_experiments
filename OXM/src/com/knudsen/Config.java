package com.knudsen;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement( name = "CONFIG" )
@XmlAccessorType(XmlAccessType.FIELD)
public class Config {

    @XmlPath("COMPLEX_THINGY/PARAMETER[@name='foo']/@value/text()")
    private int foo;

    @XmlPath("COMPLEX_THINGY/PARAMETER[@name='BAR']/@value/text()")
    private String bar;

	public int getFoo() {
		return foo;
	}

	public void setFoo(int foo) {
		this.foo = foo;
	}

	public String getBar() {
		return bar;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}
}
