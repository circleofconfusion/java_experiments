public class AnimalFactory {
    public static Animal getAnimal(String animal) {
        if (animal.equals("cat"))
            return new Cat();
        else if (animal.equals("snake"))
            return new Snake();
        else
            return new Animal();
    }
}
