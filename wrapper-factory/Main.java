public class Main {
    public static void main(String[] args) {
        Animal animal1 = AnimalFactory.getAnimal("");
        Animal cat = AnimalFactory.getAnimal("cat");
        Animal snake = AnimalFactory.getAnimal("snake");

        animal1.move();
        cat.move();
        snake.move();
    }
}
