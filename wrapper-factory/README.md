# Wrapper Factory

This experiment proves that a regular old class can be extended by subclasses
with the same method signatures. In this case, Cat and Snake are both of the
type Animal, which is also fully implemented. Cat and Snake both call their
superclass's move method, and then add on their own description of moving.

This is a potential model of how to override or customize the functionality
of certain methods of a base class while keeping the rest of its methods
unaltered.
