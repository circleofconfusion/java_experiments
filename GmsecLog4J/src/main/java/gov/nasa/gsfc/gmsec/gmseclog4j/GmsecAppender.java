package gov.nasa.gsfc.gmsec.gmseclog4j;

import gov.nasa.gsfc.gmsec.api.Config;
import gov.nasa.gsfc.gmsec.api.GMSEC_Exception;
import gov.nasa.gsfc.gmsec.api.mist.ConnectionManager;
import gov.nasa.gsfc.gmsec.api.mist.message.MistMessage;
import java.io.Serializable;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name = "GMSEC", category = "Core", elementType = "appender", printObject = true)
public class GmsecAppender extends AbstractAppender {
    
    private final String gmsecParams;
    private final String component;
    private Config config;
    private ConnectionManager cm;
    
    
    
    public GmsecAppender(final String name, final Filter filter, final Layout<? extends Serializable> layout, boolean ignoreExceptions, String gmsecParams, String component) {
        super(name, filter, layout, ignoreExceptions);
        this.gmsecParams = gmsecParams;
        this.component = component;
        try {
            config = new Config(this.gmsecParams);
            cm = new ConnectionManager(config);
            cm.initialize();
        } catch (IllegalStateException | GMSEC_Exception ex) {
            System.out.println(ex.toString());
        }
    }
    
    @PluginFactory
    public static GmsecAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginAttribute("ignoreExceptions") boolean ignoreExceptions,
            @PluginAttribute("gmsecParams") String gmsecParams,
            @PluginAttribute("component") String component,
            @PluginElement("Layout") Layout layout,
            @PluginElement("Filters") Filter filter) {
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new GmsecAppender(name, filter, layout, ignoreExceptions, gmsecParams, component);
    }

    @Override
    public void append(LogEvent le) {
        Level level = le.getLevel();
        String msgText = le.getMessage().getFormattedMessage();
        
        int severity = getSeverity(le.getLevel());

        try {
            MistMessage msg = new MistMessage(String.format("GMSEC.TEST.TEST.MSG.LOG.%s.%d", component.toUpperCase(), severity), "MSG.LOG", cm.getSpecification());
            msg.setValue("SEVERITY", severity);
            msg.setValue("MSG-TEXT", msgText);
            
            cm.publish(msg);
        } catch (IllegalStateException | GMSEC_Exception ex) {
            System.out.println(ex.toString());
        }
    }
    
    private int getSeverity(Level level) {
        if (level.intLevel() <= Level.FATAL.intLevel()) {
            return 4;
        }
        else if (level.intLevel() <= Level.ERROR.intLevel()) {
            return 3;
        }
        else if (level.intLevel() <= Level.WARN.intLevel()) {
            return 2;
        }
        else if (level.intLevel() <= Level.INFO.intLevel()) {
            return 1;
        }
        else {
            return 0;
        }
    }
    
}