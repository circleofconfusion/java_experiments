package com.knudsen.gson;

import com.google.gson.annotations.SerializedName;

public class GmsecMessage {

    @SerializedName("MESSAGE")
    private GmsecMessageDefinition message;

    public GmsecMessage() {
        // no argument constructor required by GSON
    }
	/**
	* Returns value of message
	* @return
	*/
	public GmsecMessageDefinition getMessage() {
		return message;
	}

	/**
	* Sets new value of message
	* @param
	*/
	public void setMessage(GmsecMessageDefinition message) {
		this.message = message;
	}
}
