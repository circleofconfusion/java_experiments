package com.knudsen.gson;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class GmsecMessageDefinition {
    @SerializedName("KIND")
    private String kind;

    @SerializedName("SUBJECT")
    private String subject;

    @SerializedName("FIELD")
    private List<GmsecField> field;

    public GmsecMessageDefinition() {
        // empty constructor required by GSON
    }

	/**
	* Returns value of kind
	* @return
	*/
	public String getKind() {
		return kind;
	}

	/**
	* Sets new value of kind
	* @param
	*/
	public void setKind(String kind) {
		this.kind = kind;
	}

	/**
	* Returns value of subject
	* @return
	*/
	public String getSubject() {
		return subject;
	}

	/**
	* Sets new value of subject
	* @param
	*/
	public void setSubject(String subject) {
		this.subject = subject;
	}

    public List<GmsecField> getField() {
        return field;
    }
}
