package com.knudsen.gson;

import com.google.gson.Gson;
import java.util.List;

public class GsonTest {
    public static void main(String[] args) {
        Gson gson = new Gson();

        GmsecMessage gmsecMessage = gson.fromJson("{\"MESSAGE\":{\"KIND\":\"PUBLISH\",\"SUBJECT\":\"GMSEC.TEST-MISSION-ID.SATID.MSG.C2CX.GSS-ATLAS.HB\",\"FIELD\":[{\"NAME\":\"C2CX-SUBTYPE\",\"TYPE\":\"STRING\",\"VALUE\":\"HB\"}]}}", GmsecMessage.class);

        System.out.println("Kind:    " + gmsecMessage.getMessage().getKind());
        System.out.println("Subject: " + gmsecMessage.getMessage().getSubject());
        GmsecField field1 = gmsecMessage.getMessage().getField().get(0);
        System.out.println("Field1 name: " + field1.getName());
        System.out.println("Field1 type: " + field1.getType());
        System.out.println("Field1 value: " + field1.getValue());
        field1.setValue("DOUBLEPLUSGOODHB");

        System.out.println(gson.toJson(gmsecMessage));
    }
}
