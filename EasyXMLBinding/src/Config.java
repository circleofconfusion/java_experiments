import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement( name = "CONFIG" )
@XmlAccessorType( XmlAccessType.PROPERTY ) // annotating the setters with @XmlElement annotations
public class Config {

    private ComplexThingy complexThingy;

	/**
	* Returns value of complexThingy
	* @return
	*/
	public ComplexThingy getComplexThingy() {
		return complexThingy;
	}

	/**
	* Sets new value of complexThingy
	* @param
	*/
    @XmlElement(name="COMPLEX_THINGY")
	public void setComplexThingy(ComplexThingy complexThingy) {
		this.complexThingy = complexThingy;
	}


    // private int foo;
    // private String bar;
    //
	// /**
	// * Returns value of foo
	// * @return
	// */
	// public int getFoo() {
	// 	return foo;
	// }
    //
	// /**
	// * Sets new value of foo
	// * @param
	// */
    // @XmlElement( name = "foo" )
	// public void setFoo(int foo) {
	// 	this.foo = foo;
	// }
    //
	// /**
	// * Returns value of bar
	// * @return
	// */
	// public String getBar() {
	// 	return bar;
	// }
    //
	// /**
	// * Sets new value of bar
	// * @param
	// */
    // @XmlElement( name="BAR" )
	// public void setBar(String bar) {
	// 	this.bar = bar;
	// }
}
