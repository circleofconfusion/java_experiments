import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement(name="COMPLEX_THINGY")
@XmlType( propOrder = { "foo", "bar" } )
@XmlAccessorType( XmlAccessType.FIELD ) // annotating the fields with @XmlElement annotations
public class ComplexThingy {

    @XmlElement
    private int foo;

    @XmlElement(name="BAR")
    private String bar;

	/**
	* Returns value of foo
	* @return
	*/
	public int getFoo() {
		return foo;
	}

	/**
	* Sets new value of foo
	* @param
	*/
    // @XmlElement( name = "FOO" )
	public void setFoo(int foo) {
		this.foo = foo;
	}

	/**
	* Returns value of bar
	* @return
	*/
	public String getBar() {
		return bar;
	}

	/**
	* Sets new value of bar
	* @param
	*/
    // @XmlElement( name = "BAR" )
	public void setBar(String bar) {
		this.bar = bar;
	}

}
