import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class UnmarshalTest {
    public static void main(String args[]) throws JAXBException {
        File configFile = new File("config.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance(Config.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Config config = (Config) jaxbUnmarshaller.unmarshal(configFile);

        System.out.println("Foo: " + config.getFoo());
        System.out.println("BAR: " + config.getBar());
    }
}
