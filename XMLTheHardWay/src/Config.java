import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement( name = "CONFIG" )
@XmlType( propOrder = { "foo", "bar" } )
@XmlAccessorType( XmlAccessType.PROPERTY )
public class Config {

    @XmlPath("COMPLEX_THINGY/PARAMETER[@name='foo']/@value")
    private int foo;

    @XmlPath("COMPLEX_THINGY/PARAMETER[@name='BAR']/@value")
    private String bar;

	/**
	* Returns value of foo
	* @return
	*/
	public int getFoo() {
		return foo;
	}

	/**
	* Sets new value of foo
	* @param
	*/
	public void setFoo(int foo) {
		this.foo = foo;
	}

	/**
	* Returns value of bar
	* @return
	*/
	public String getBar() {
		return bar;
	}

	/**
	* Sets new value of bar
	* @param
	*/
	public void setBar(String bar) {
		this.bar = bar;
	}
}
