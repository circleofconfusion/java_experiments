# TryCatchFinallyTest

This is simply a test to see if a return statement in an Exception handler will prevent a finally block from executing.

Short answer: The finally block will _always_ execute, even if the exception handler has a return statement.
