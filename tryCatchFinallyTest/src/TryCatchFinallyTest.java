import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;

public class TryCatchFinallyTest {

    public static void main(String args[]) {

        File f = null;
        BufferedReader br = null;
        try {
            f = new File("bogusFileName.xml");
            br = new BufferedReader(new FileReader(f));
        }
        catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
            return;
        }
        finally {
            try {
                System.out.println("Closing BufferedReader");
                if (br != null) br.close();
            }
            catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

    }

}
