package com.knudsen.log4jexperiment;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);
        
    public static void main(String[] args) throws InterruptedException {
        System.out.println(System.getProperty("java.library.path"));
        while(true) {
            logger.debug("Another DEBUG entry");
            logger.info("Another INFO entry");
            logger.warn("Another WARN entry");
            logger.error("Another ERROR entry");
            logger.fatal("Another FATAL entry");
            Thread.sleep(1000);
        }
    }
    
}
