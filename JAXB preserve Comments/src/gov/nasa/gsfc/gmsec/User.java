/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.nasa.gsfc.gmsec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author stknudse
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
    
    @XmlElement
    String username;
    
    @XmlElement
    String password;
    
    @XmlElement(name = "optional-data")
    OptionalData optionalData;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public OptionalData getOptionalData() {
        return optionalData;
    }

    public void setOptionalData(OptionalData optionalData) {
        this.optionalData = optionalData;
    }
    
}
