/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.nasa.gsfc.gmsec;

import java.io.File;
import java.io.IOException;
import javax.xml.bind.Binder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.codec.digest.DigestUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author stknudse
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, JAXBException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File xml = new File("xmlFiles/xmlWithComments.xml");
        Document document = (Document) db.parse(xml);
        
        JAXBContext jc = JAXBContext.newInstance(User.class);
 
        Binder<Node> binder = jc.createBinder();
        binder.setProperty("jaxb.formatted.output", true);
        
        User u = (User) binder.unmarshal(document);
        
        u.setPassword(DigestUtils.sha1Hex(u.getPassword()));
        if(u.getOptionalData() != null) {
            u.getOptionalData().setAddress("543 Some other Street");
        }
        
        
        // put the comments and unbound elements back
        binder.updateXML(u);
 
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        t.transform(new DOMSource(document), new StreamResult(System.out));
        t.transform(new DOMSource(document), new StreamResult(xml));
    }
    
}
